import React from "react";
import axios from "axios";

const addTodo = async ( todoObject ) => {
    // console.log(todoObject.title);
    return await axios({
        method: "post",
        url: "/api/todos",
        data: {
            title: todoObject.title,
            description: todoObject.description
        }
    });
};

const updateTodo = async ( todoObject, id ) => {
    id = id.id;
    return await axios({
        method: "put",
        url: "/api/todos/"+id,
        data: {
            title: todoObject.title,
            description: todoObject.description
        }
    });
};

const deleteTodo = async ( id ) => {
   // console.log(id);
    return await axios.delete("api/todos/"+id)
};

const getTodos = async () => {
    return await axios.get("/api/todos");
};

const getTodoById = ({ id }) => {};

export { getTodoById, getTodos, deleteTodo, updateTodo, addTodo };
