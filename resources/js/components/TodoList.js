import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import { getTodos } from "../data/api";
import { deleteTodo } from "../data/api";
import { useHistory } from 'react-router-dom';
import Table from 'react-bootstrap/Table';
import AutoScale from 'react-auto-scale';

function TodoList(props) {
    const [todos, setTodos] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        todoList();
    }, []);

    const todoList = () => {
        setLoading(true);
        getTodos()
            .then(res => {
                console.log(res);
                setTodos(res.data);
                setLoading(false);
            })
            .catch(err => {
                setLoading(false);
            });
    };

    let history = useHistory();

    const renderBody = () => {
        return todos.map(todo => {
            return (
                <tr key={todo.id}>
                    <td scope="row">{todo.id}</td>
                    <td>{todo.title}</td>
                    <td>{todo.description}</td>
                    <td><button className="btn btn-primary" onClick={() => props.history.push('/'+todo.id)}>Update</button>
                        <button className="btn btn-primary" onClick={() => deleteTodo(todo.id).then(() => {
                            window.location.reload(false);
                        })}>Delete</button>
                    </td>
                </tr>
            );
        });
    };

    return (
        <div className="main">
            <div className="navbar navbar-light bg-light justify-content-center">
                <h1 className={`mb-5`}>Todo App</h1>
            </div>
            <div className="row justify-content-center">
                <Table striped bordered hover size="sm">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Description</th>
                            <th scope="col">Action</th>
                            {/* <th scope="col">Handle</th> */}
                        </tr>
                    </thead>
                    <tbody>
                    {loading && <tr><td colSpan='4' className={`{h4}`}>...loading</td></tr>}
                        {renderBody()}
                    </tbody>
                </Table>
                <button className="btn btn-primary" onClick={() => {props.history.push('/create')}}>Add</button>
            </div>
        </div>
    );
}

export default TodoList;
