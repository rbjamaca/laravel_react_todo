import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom"
import { isNull } from "lodash";
import { updateTodo } from "../data/api";
import { isEmpty } from "../helper/inputHelper";

function UpdateTodo(props){

    let id = props.match.params;
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");

    const handleFieldChange = ( text, field ) => {
        if (!isEmpty(text) && field === "title") {
            setTitle(text.target.value);
        }

        if (!isEmpty(text) && field === "desc") {
            setDescription(text.target.value);
        }
    };

    const clearFields = () => {
        setTitle("");
        setDescription("");
    };

    

    return (
        <div className="main">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Update Todo</div>
                        <div className="input-group">
                            <input
                                type="text"
                                name="uptitle"
                                placeholder="Title"
                                value={title}
                                onChange={text => handleFieldChange(text, 'title')}
                            />
                            <input
                                type="text"
                                name="updescription"
                                placeholder="Description"
                                value={description}
                                onChange={text => handleFieldChange(text, 'desc')}
                            />
                            <div className="input-group-append">
                                <button className="btn btn-primary"
                                onClick={() => 
                                    updateTodo({
                                        title: title,
                                        description: description
                                    }, id).then(() => {
                                        props.history.push('/')
                                    })}
                                    >Update</button>
                                <button
                                    onClick={clearFields}
                                    className="btn btn-primary"
                                >
                                    Back
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default UpdateTodo;
