export const isEmpty = str => {
    return !str || 0 === str.length;
};
